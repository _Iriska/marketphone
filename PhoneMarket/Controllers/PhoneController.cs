﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using PhoneMarket.Models;
using PhoneMarket.Providers;

namespace PhoneMarket.Controllers
{
    public class PhoneController : ApiController
    {
        [HttpGet]
        public JsonResult<List<Phone>> GetAll()
        {
            using (var context = new MssqlProvider())
            {
                return Json(context.Phones.ToList());
            }
        }

        [HttpPost]
        public Phone Add(Phone phone)
        {
            using (var context = new MssqlProvider())
            {
                context.Phones.Add(phone);
                context.SaveChanges();

                return phone;
            }
        }

        [HttpPut]
        public Phone Update(Phone phone)
        {
            using (var context = new MssqlProvider())
            {
                context.Phones.AddOrUpdate(phone);
                context.SaveChanges();

                return phone;
            }
        }

        [HttpDelete]
        public void Remove(int? id)
        {
            using (var context = new MssqlProvider())
            {
                context.Phones.Remove(context.Phones.FirstOrDefault(phone => phone.Id == id));
                context.SaveChanges();
            }
        }
    }
}