﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.Results;
using PhoneMarket.Models;
using PhoneMarket.Providers;

namespace PhoneMarket.Controllers
{
    public class CategoryController : ApiController
    {
        [HttpGet]
        public JsonResult<List<Category>> GetAll()
        {
            using (var context = new MssqlProvider())
            {
                return Json(context.Categories.ToList());
            }
        }

        [HttpPost]
        public Category Add(Category category)
        {
            using (var context = new MssqlProvider())
            {
                context.Categories.Add(category);
                context.SaveChanges();

                return category;
            }
        }

        [HttpPut]
        public Category Update(Category category)
        {
            using (var context = new MssqlProvider())
            {
                context.Categories.AddOrUpdate(category);
                context.SaveChanges();

                return category;
            }
        }

        [HttpDelete]
        public void Remove(int? id)
        {
            using (var context = new MssqlProvider())
            {
                context.Categories.Remove(context.Categories.FirstOrDefault(category => category.Id == id));
                context.SaveChanges();
            }
        }
    }
}