﻿
using System;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace PhoneMarket.Binders.Providers
{
    public class CategoryModelBinderProvider : ModelBinderProvider
    {
        public override IModelBinder GetBinder(HttpConfiguration configuration, Type modelType)
        {
            return new CategoryModelBinder();
        }
    }
}