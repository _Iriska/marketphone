﻿using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using PhoneMarket.Models;

namespace PhoneMarket.Binders
{
    public class CategoryModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var model = (Category)bindingContext.Model ?? new Category();

            var hasPrefix = bindingContext.ValueProvider.ContainsPrefix(bindingContext.ModelName);

            var searchPrefix = (hasPrefix) ? bindingContext.ModelName + "." : "";

            int id;

            if (int.TryParse(GetValue(bindingContext, searchPrefix, "Id"), out id))
            {
                model.Id = id;
            }

            model.Name = GetValue(bindingContext, searchPrefix, "Name");
            model.Description = GetValue(bindingContext, searchPrefix, "Description");

            bindingContext.Model = model;

            return true;
        }

        private string GetValue(ModelBindingContext context, string prefix, string key)
        {
            var result = context.ValueProvider.GetValue(prefix + key);
            return result == null ? null : result.AttemptedValue;
        }
    }
}