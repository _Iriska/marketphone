﻿using System.Data.Entity;
using PhoneMarket.Models;

namespace PhoneMarket.Providers
{
    public class MssqlProvider : DbContext
    {
        public MssqlProvider() : base("DefaultConnection") { }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Phone> Phones { get; set; }
    }
}