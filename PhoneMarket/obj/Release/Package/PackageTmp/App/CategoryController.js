﻿application.controller('CategoryController', ['$scope', 'CategoryService', function ($scope, categoryService) {
    $scope.addCategory = function () {
        categoryService.add({Id: 0, Name: $scope.name, Description: $scope.description, Phones: []});

        $scope.$close();
    };
}]);
