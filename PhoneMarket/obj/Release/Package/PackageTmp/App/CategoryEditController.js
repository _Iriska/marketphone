﻿application.controller('CategoryEditController', [
    '$scope', 'PhonesAndCategoriesService', 'currentCategory', function ($scope, phonesAndCategoriesService, currentCategory) {
        $scope.phones = phonesAndCategoriesService.getPhones();
        $scope.categories = phonesAndCategoriesService.getCategories();

        var editCategory;
        
        editCategory = currentCategory;

        //запись в модель
        $scope.name = editCategory.name;
        $scope.description = editCategory.description;

        $scope.addCategory = function () {
            editCategory.name = $scope.name;
            editCategory.description = $scope.description;

            $scope.$close();
        };

    }]);