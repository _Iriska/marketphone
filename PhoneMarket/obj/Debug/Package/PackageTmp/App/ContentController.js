﻿application.controller('ContentController', ['$scope', '$uibModal', 'PhonesAndCategoriesService', 'PhoneService', 'CategoryService', function ($scope, $uibModal, phonesAndCategoriesService, phoneService, categoryService) {
    $scope.animationsEnabled = true;

    $scope.addCategory = function () {
        $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: '../Views/category.html',
            controller: 'CategoryController',
            resolve: {
                currentCategory: function () {
                    return $scope.selectedItem;
                }
            }
        });
    };

    var renderTreePhone = function () {
        $scope.treeData = [];

        categoryService.getAll().then(function (categoriesRaw) {
            phoneService.getAll().then(function (phonesRaw) {
                (categoriesRaw || []).forEach(function (category) {
                    $scope.treeData.push({ id: category.Id, Name: category.Name, children: _.filter(phonesRaw, { 'Id': category.Id }) });
                });
            });
        });
    };

    renderTreePhone();
}]);