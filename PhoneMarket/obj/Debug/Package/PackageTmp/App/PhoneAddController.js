﻿application.controller('PhoneAddController', [
    '$scope', 'PhonesAndCategoriesService', 'currentCategory', function ($scope, phonesAndCategoriesService, $uibModalInstance, currentCategory) {
        $scope.phones = phonesAndCategoriesService.getPhones();
        $scope.categories = phonesAndCategoriesService.getCategories();
        $scope.canEdit = window.location.hash === '#/phone/edit';


        var editCategory;
        editCategory = currentCategory;
        //debugger;
        // Задание: если выделение есть на категории // телефоне, сделать так, чтобы 
        // в поле Категория у нового телефона по умолчанию было название этой категории


        // Открытый баг: при редактировании категории, все изменения не добавляются в сервис

        //$scope.category = editCategory.name;











        //if ($scope.canEdit) {
        //    alert('!!!!');
        //    for (var i = 0; i < $scope.phones.length; i++) {
        //        var phone = $scope.phones[i];
        //        if (phone.selected) {
        //            var keys = Object.keys(phone);
        //            keys.forEach(function (key) {
        //                $scope[key] = phone[key];
        //            });

        //            $scope.selectedPhone = phone;

        //            for (var j = 0; j < $scope.categories.length; i++) {
        //                if ($scope.categories[i].id === phone.categoryId)
        //                    $scope.selectedCategory = $scope.categories[i];
        //                break;
        //            }
        //        }
        //        break;
        //    }
        //}

        $scope.submit = function () {
            if ($scope.canEdit) {
                var keys = Object.keys($scope.selectedPhone);
                keys.forEach(function(key) {
                    $scope.selectedPhone[key] = $scope[key];
                });
            } else {
                phonesAndCategoriesService.addPhone({
                    type: 'phone',
                    name: $scope.name,
                    description: $scope.description,
                    photoUrl: $scope.photoUrl,
                    category: $scope.selectedCategory.name,
                    categoryId: $scope.selectedCategory.id
                });
            }
            $scope.$close();
        };
        
    }]);
