﻿var application = angular.module("application", ["treeControl", "ngRoute", "ngAnimate", "ui.bootstrap"])
    //.config(function ($routeProvider) {
    .config([
        "$routeProvider", function ($routeProvider) {

            $routeProvider
                .when("/phone/:phoneId", {
                    templateUrl: "Views/phoneTemlplate.html",
                    controller: "PhoneDetailsController"
                });

            $routeProvider
                .when("/category/:categoryId", {
                    templateUrl: "Views/categoryTemlplate.html",
                    controller: "CategoryDetailsController"
                });

            $routeProvider.otherwise({
                templateUrl: "Views/default.html"
            });
        }
    ]).service("PhonesAndCategoriesService", function () {

        this.phones = [
            { id: 0, type: "phone", name: "Push 180 Dual", description: "Общая информация: Дата выхода на рынок: 2012 г.", photoUrl: "../img/QUMOPush180Dual.jpg", category: "QUMO", categoryId: 0 },
            { id: 1, type: "phone", name: "TM-99 Black", description: "Дата выхода на рынок: 2015 г.", photoUrl: "../img/TM-99Black.jpg", category: "TeXet", categoryId: 1 },
            { id: 2, type: "phone", name: "Larus E1", description: "Тип: телефон", photoUrl: "../img/LarusE1.jpg", category: "DEXP", categoryId: 2 },
            { id: 3, type: "phone", name: "Push 184", description: "Дата выхода на рынок: 2014 г.", photoUrl: "../img/Push184.jpg", category: "QUMO", categoryId: 0 }
        ];

        this.selectedCategory = null;

        this.categories = [
           { id: 0, type: "categoryPhone", name: "QUMO", description: "Южнокорейская группа компаний, один из крупнейших в Южной Корее чеболей, основанный в 1938 году." },
           { id: 1, type: "categoryPhone", name: "TeXet", description: "Российская марка потребительской электроники, основанная в 2004 году. Торговая марка принадлежит компании «Электронные системы Алкотел»." },
           { id: 2, type: "categoryPhone", name: "DEXP", description: "Компания, владелец розничной сети, специализирующейся на продаже компьютерной, цифровой и бытовой техники, а также производитель компьютеров, в том числе ноутбуков и планшетов (сборочное производство)" }
        ];

        this.addPhone = function (phone) {
            phone.id = this.getNextPhoneId();
            this.phones.push(phone);
        };
        this.addCategory = function (category) {
            category.id = this.getNextCategoryId();
            this.categories.push(category);
        };

        this.getPhones = function () {
            return this.phones;
        };

        this.getCategories = function () {
            return this.categories;
        };

        this.getNextPhoneId = function () {
            return this.getNextId(this.phones);
        };

        this.getNextCategoryId = function () {
            return this.getNextId(this.categories);
        };

        this.getNextId = function (array) {
            var maxIndex = -1;
            array.forEach(function (item) {
                if (maxIndex < item.id) {
                    maxIndex = item.id;
                }
            });

            return maxIndex + 1;
        };

        this.removePhone = function (phone) {
            //debugger;
            for (var indexPhone = 0; indexPhone < this.phones.length; indexPhone++) {
                if (this.phones[indexPhone].id === phone.id) {
                    this.phones.splice(indexPhone, 1);


                    break;
                }
            }
        };

        //                                                     Удаляю категорию
        this.removeCategory = function (category) {
            //debugger;
            for (var indexCategory = 0; indexCategory < this.categories.length; indexCategory++) {
                //                                               Нахожу нужную категорию
                if (this.categories[indexCategory].id === category.id) {

                    // Сначала удаляю телефоны
                    for (var indexPhone = 0; indexPhone < this.phones.length; indexPhone++) {
                        if (this.phones[indexPhone].categoryId === this.categories[indexCategory].id) {
                            this.phones.splice(indexPhone, 1);
                        }
                    }
                    this.categories.splice(indexCategory, 1);
                    break;
                }
            }
        };
    }).service("PhoneService", ['$http', function ($http) {
        this.getAll = function () {
            return $http.get('/api/phone/GetAll').then(function (response) {
                return response.data;
            });
        };
    }]).service("CategoryService", ['$http', function ($http) {
        this.getAll = function () {
            return $http.get('/api/category/GetAll').then(function (response) {
                return response.data;
            });
        };

        this.add = function (category) {
            return $http.post('/api/category/add', JSON.stringify(category), {
                headers: { 'Content-Type': 'application/json;charset=utf-8' }
            }).then(function (response) {
                return response.data;
            });
        };
    }]);
