﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace PhoneMarket.Models
{
    [Serializable]
    public class Phone
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string PhotoUrl { get; set; }

        [JsonIgnore]
        public virtual Category Category { get; set; }
    }
}