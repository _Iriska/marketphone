﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Http.ModelBinding;
using Newtonsoft.Json;
using PhoneMarket.Binders;

namespace PhoneMarket.Models
{
    [ModelBinder(typeof(CategoryModelBinder))]
    [Serializable]
    public class Category
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [JsonIgnore]
        public virtual List<Phone> Phones { get; set; }
    }
}