﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Newtonsoft.Json;

namespace PhoneMarket
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var settings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                Culture = CultureInfo.InvariantCulture,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                //ContractResolver = new CamelCasePropertyNamesContractResolver(),
            };

            //HttpConfiguration config = GlobalConfiguration.Configuration;
            //config.Formatters.JsonFormatter.SerializerSettings.Converters.Add
            //     (new StringEnumConverter {CamelCaseText = true});

            JsonConvert.DefaultSettings = () => settings;

            GlobalConfiguration.Configure(WebApiConfig.Register);

          

        }
    }
}
